@extends('layout.app')

@section('content')

    <cart-component :products="{{ session()->has('cart')? json_encode(session()->get('cart')) : '[]' }}" />

@endsection
