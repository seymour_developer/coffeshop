<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['firstname', 'lastname', 'phone', 'sum', 'address', 'email', 'note'];

    public function items() {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }
}
