<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request) {

        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string|size:13',
            'email' => 'required|email',
        ]);

        if (session()->has('cart')) {

            $cart = session()->get('cart');
            $sum = 0;
            foreach ($cart as $product) {
                $sum += $product['price'] * $product['quantity'];
            }

            $inputs = $request->except('_token');
            $inputs['sum'] = $sum;

            $order = Order::create($inputs);

            if (!empty($order)) {

                foreach ($cart as $item) {
                    OrderItem::create([
                        'product_id' => $item['product_id'],
                        'qty' => $item['quantity'],
                        'order_id' => $order->id,
                        'sum' => $item['quantity'] * $item['price'],
                    ]);
                }
            }
            return back()->with('success', 'Your order was successfully places!');
        }
        return back()->with('error', 'Something went wrong!');
    }
 }
