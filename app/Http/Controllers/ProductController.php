<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function cart() {
        return view('cart');
    }

    public function addToCart(Product $product) {

        $cart = session()->get('cart');

        if (!$cart) {

            $cart = [
                $product->id => [
                    'product_id' => $product->id,
                    'title' => $product->title,
                    'price' => $product->price,
                    'image' => $product->image,
                    'quantity' => 1
                ]
            ];
        }
        else if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity']++;
        }
        else {
            $cart[$product->id] = [
                'product_id' => $product->id,
                'title' => $product->title,
                'price' => $product->price,
                'image' => $product->image,
                'quantity' => 1
            ];
        }

        session()->put('cart', $cart);
        $cart = session()->get('cart');

        return response()->json([
            'message' => 'Product added to cart successfully',
            'data' => $this->sumAndQty()
        ], 200);
    }

    public function removeFromCart(Request $request) {
        if ($request->id) {
            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);

                session()->put('cart', $cart);

                $cart = session()->get('cart');
                return response()->json([
                    'message' => 'Product removed from cart',
                    'data' => $this->sumAndQty()
                ], 200);
            }
        }
        return response()->json(['message' => 'Cart update failed'], 400);
    }

    public function updateCart(Request $request) {
        if ($request->id && $request->quantity && $request->quantity>0) {

            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {
                $cart[$request->id]['quantity'] = $request->quantity;

                session()->put('cart', $cart);

                $cart = session()->get('cart');
                return response()->json([
                    'message' => 'Cart was updated successfully',
                    'data' => $this->sumAndQty()
                ], 200);
            }
        }
        return response()->json(['message' => 'Cart update failed'], 400);
    }

    public function sumAndQty() {
        $cart = session()->get('cart');
        $sum = 0;
        $qty = 0;
        foreach ($cart as $item) {
            $sum += $item['price'] * $item['quantity'];
            $qty += $item['quantity'];
        }
        return [
            'sum' => $sum,
            'qty' => $qty
        ];
    }
}
