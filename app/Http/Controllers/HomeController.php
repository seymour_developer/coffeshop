<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {

        $products = Product::select('id', 'title', 'price', 'image')->get();
        return view('home', compact('products'));
    }
}
