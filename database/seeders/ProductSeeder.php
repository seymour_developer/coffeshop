<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        
        $data = [
            [
                'title' => 'Cappuccino',
                'price' => rand(12, 57) / 10,
                'image' => 'cappuccino.png',
            ],
            [
                'title' => 'Latte',
                'price' => rand(12, 57) / 10,
                'image' => 'latte.png',
            ],
            [
                'title' => 'Americano',
                'price' => rand(12, 57) / 10,
                'image' => 'americano.png',
            ],
            [
                'title' => 'Espresso',
                'price' => rand(12, 57) / 10,
                'image' => 'espresso.png',
            ],
            [
                'title' => 'Turkish',
                'price' => rand(12, 57) / 10,
                'image' => 'turkish.png',
            ]
        ];
        
        DB::table('products')->insert($data);
    }
}
